CFLAGS = /nologo /W3 

build: tema1.exe

tema1.exe: tema1.obj
	link tema1.obj compare.lib 

tema1.obj: tema1.c
	cl /c tema1.c 


clean:
	del /Q /F *.obj tema1.exe

