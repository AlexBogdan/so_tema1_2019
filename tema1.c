/* SPDX-License-Identifier: MIT */
/* Autor : Tragic Solitude - 000 Monitor  */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "compare.h"

#define ENOMEM 12
#define BUFFER_SIZE 200005

/* Definim un nod din coada de prioritati */
typedef struct node {
	char *text;
	int priority;
	struct node *next;
} node;

/* Vom avea nevoie doar de nodul de start pentru coada */
typedef struct priority_queue {
	struct node *head;
} priority_queue;

/* Constructori / Destructori pentru structuri */
static node *create_node(const char *text, int priority);
static void destroy_node(node *n);
static priority_queue *create_queue();
static void destroy_queue(priority_queue *queue);

/* Functii cu care accesam coada */
static int insert_element(
	priority_queue *queue,
	const char *text,
	int priority
);
static char *get_top_element(priority_queue *queue);
static int remove_top_element(priority_queue *queue);

/* Citire si parsare comenzi */
static int command_type(const char *cmd);
static int check_insert_cmd(const char *cmd);
static int get_cmds(priority_queue *queue, FILE *input);
static int execute_command(priority_queue *queue, char *cmd);


/* Cream un nod nou cu informatia primita */
static node *create_node(const char *text, int priority)
{
	node *n;

	n = (node *) malloc(sizeof(node));
	if (n == NULL)
		return NULL;
	n->next = NULL;
	n->priority = priority;
	n->text = (char *) malloc(sizeof(char) * (strlen(text) + 1));
	if (n->text == NULL)
		return NULL;
	strcpy(n->text, text);

	return n;
}

/* Eliberam memoria ocupata de nod */
static void destroy_node(node *n)
{
	free(n->text);
	free(n);
}

/* Alocam spatiu pentru coada de prioritati */
static priority_queue *create_queue()
{
	priority_queue *queue;

	queue = (priority_queue *) malloc(sizeof(priority_queue));
	if (queue == NULL)
		return NULL;
	queue->head = NULL;

	return queue;
}

/* Stergem toate nodurile cozii */
static void destroy_queue(priority_queue *queue)
{
	node *it;

	for (it = queue->head; it != NULL; it = queue->head) {
		queue->head = queue->head->next;
		destroy_node(it);
	}
	free(queue);
}

/* Inseram un nod nou in coada in functie de prioritate */
static int insert_element(priority_queue *queue, const char *text, int priority)
{
	node *n, *crr_it, *prev_it;

	n = create_node(text, priority);
	if (n == NULL)
		return -ENOMEM;

	/* Daca coada este momentan goala, inseram direct in head */
	if (queue->head == NULL)
		queue->head = n;
	else {
		/* Verificam unde vom adauga noul nod in coada */
		for (
				crr_it = prev_it = queue->head;
				crr_it != NULL;
				prev_it = crr_it, crr_it = crr_it->next
			) {
			/* Adaugam nodul cand gasim prioritatea potrivita */
			if (compare(n->priority, crr_it->priority) > 0) {
				if (crr_it == queue->head) {
					n->next = crr_it;
					queue->head = n;
				} else {
					prev_it->next = n;
					n->next = crr_it;
				}
				return 0;
			}
		}
		/*
		 *	Nu am adaugat inca nodul, inseamna ca are cea mai mica
		 * prioritate, deci il vom adauga la finalul cozii
		 */
		prev_it->next = n;
	}

	return 0;
}

/* Intoarcem elementul cu prioritatea cea mai mare, daca exista */
static char *get_top_element(priority_queue *queue)
{
	if (queue->head == NULL)
		return "\n";
	return queue->head->text;
}

/* Scoatem din coada elementul cu prioritatea cea mai mare */
static int remove_top_element(priority_queue *queue)
{
	node *n;

	/* In cazul in care coada este goala, nu facem nimic */
	if (queue->head == NULL)
		return 0;

	n = queue->head;
	/* Actualizam head-ul cozii in urma eliminarii */
	queue->head = queue->head->next;
	/* Eliberam memoria ocupata de acest nod */
	destroy_node(n);

	return 0;
}

/* Verificam ce tip de comanda am primit */
static int command_type(const char *cmd)
{
	if (strstr(cmd, "insert") == cmd)
		return 1;
	if (strcmp(cmd, "top") == 0)
		return 2;
	if (strcmp(cmd, "pop") == 0)
		return 3;

	return 0;
}

/* Verificam daca avem o comanda de forma 'insert string int' */
static int check_insert_cmd(const char *cmd)
{
	char *text, *prio, *result;

	/* Ne asiguram ca stringul exista */
	text = strchr(cmd, ' ');
	if (text == NULL)
		return 1;
	text++;

	/* Ne asiguram ca prioritatea exista */
	prio = strchr(text, ' ');
	if (prio == NULL)
		return 2;
	prio++;

	/* Ne asiguram ca prioritatea este un numar */
	strtol(prio, &result, 10);
	if (*result != '\0')
		return 3;

	return 0;
}

/* Executam comanda primita */
static int execute_command(priority_queue *queue, char *cmd)
{
	char *text, *prio;

	switch (command_type(cmd)) {
	case 1:
	/* Daca 'insert' este o comanda valida, o parsam si executam */
		if (check_insert_cmd(cmd) == 0) {
			text = strchr(cmd, ' ') + 1;
			prio = strchr(text, ' ') + 1;
			*(prio - 1) = '\0';

			return insert_element(queue, text, atoi(prio));
		}

		break;
	case 2:
		printf("%s\n", get_top_element(queue));
		break;
	case 3:
		remove_top_element(queue);
		break;
	/* Orice alta comanda este ignorata */
	}

	return 0;
}


/* Citim comenzile din input (stdin sau fisier) */
static int get_cmds(priority_queue *queue, FILE *input)
{
	char buf[BUFFER_SIZE];

	/* Cat timp putem citi, executam comenzile primite */
	while (fgets(buf, BUFFER_SIZE, input) != NULL) {
		buf[strlen(buf) - 1] = '\0';
		if (execute_command(queue, buf) == -ENOMEM)
			return -ENOMEM;
	}

	return 0;
}

int main(int argc, char *argv[])
{
	priority_queue *queue;
	int val, i;
	FILE *input_file;

	queue = create_queue();
	if (queue == NULL)
		return ENOMEM;

	/* Verificam daca citim din fisiere sau de la stdin */
	if (argc == 1) {
		if (get_cmds(queue, stdin) == -ENOMEM)
			return ENOMEM;
	} else
		for (i = 1; i < argc; i++) {
			/* Daca fisierul exista, executam comenzile din el */
			input_file = fopen(argv[i], "r");
			if (input_file != NULL) {
				val = get_cmds(queue, input_file);
				fclose(input_file);
				/* Daca au aparut probleme ne oprim */
				if (val == -ENOMEM)
					return ENOMEM;
			}
		}

	destroy_queue(queue);
	return 0;
}
