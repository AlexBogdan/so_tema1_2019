Tema 1 SO
Tragic Solitude
000 Monitor

    Implementarea cozii de prioritati a constat in crearea unor structuri care
sa-mi permita sa tin in memorie, alocate dinamic, stringurile primite.
Pentru a fi mai clara implementarea acestora, am creat constructori si
destructori, astfel incat memoria sa fie gestionata mai clar (evitare memory
leaks)

    Cele 3 operatii pe coada vor fi tratate in cate o functie:

    1. Insert (insert_element) -> Vom crea un nod nou in care copiem informatia
primita. In cazul in care coada este goala, acesta va deveni direct 'head'-ul
cozii. Daca mai avem elemente in coada, iteram prin aceasta pentru a gasi locul
potrivit in care sa inseram noul nod (in functie de prioritate).
    2. Top (get_top_element) -> Intoarce elementul cu prioritate maxima sau '\n'
daca nu avem niciun element in coada.
    3. Pop (remove_top_element) -> Eliminam elementul cu prioritate maxima, daca
acesta exista.

    Pentru a citi comenzile vom verifica numarul de parametri primiti. In cazul
in care avem 0, atunci citirea comenzilor se va face din 'stdin', altfel vom
itera prin parametri primiti in linie de comanda si vom incerca sa deschidem
fisierele cu comenzi (sarindu-le daca nu exista).
    Comenzile citite vor fi verificate in functia (command_type) pentru a le
stabili tipul.
    In cazul in care avem o comanda 'insert', o verificare suplimentara a
parametrilor acesteia va fi efectuata in functia (check_insert_cmd).

    Daca avem erori in momentul alocarilor, acestea vor fi intoarse pana in
functia main (de exemplu ENOMEM).

    In finalul executiei programului vom elibera memoria ocupata de elementele
ramase in coada.
